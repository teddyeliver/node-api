const express = require('express');
const Product = require('./models/ProductModel')
const mongoose = require('mongoose');

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: false}))

// routes
app.get('/', (req, res) => {
    res.send('test send response')
})

app.get('/blog', (req, res) => { 
    res.send('test blog here')
})

app.get('/products', async(req, res) => {
    try {
        const products = await Product.find({});
        res.status(200).json(products)
    } catch (error) {
        console.log(error.message);
        res.status(500).json({message: error.message})
    }
})

app.get('/products/:id', async(req, res) => {
    try {
        const {id} = req.params;
        const product = await Product.findById(id);
        res.status(200).json(product)
    } catch (error) {
        console.log(error.message);
        res.status(500).json({message: error.message});
    }
})

app.post('/products', async(req, res) => {
    try {
        const product = await Product.create(req.body)
        res.status(200).json(product);
    } catch (error) {
        console.log(error.message);
        res.status(500).json({message: error.message})
    }
})

// update products
app.put('/product/:id', async(req, res) => {
    try {
        const {id} = req.params;
        const product = await Product.findByIdAndUpdate(id, req.body)
        if (!product) {
            return res.status(404).json({message: `Product with ID ${id} not found!`})
        }
        const updateProduct = await Product.findById(id)
        res.status(200).json(updateProduct)
    } catch (error) {
        res.status(500).json({message: error.message});
    }
})
// delete product
app.delete('/product/:id', async(req, res) => {
    try {
        const {id} = req.params;
        const product = await Product.findByIdAndDelete(id)
        if (!product) {
            return res.status(404).json({message: `Product with the ID ${id} not found!`})
        }
        res.status(200).json(product)
    } catch (error) {
        res.status(500).json({message: error.message})
    }
})

/**
 * code will be modified like this later
 */
// USE MIDDLEWARE
const logger = require('./middleware/logger');
app.use(logger);

// USE ROUTES
const userRoutes = require('./routes/userRoutes');
app.use('/api', userRoutes);

mongoose.set('strictQuery', false)
mongoose.
connect('mongodb+srv://teddyeliver:tedted123@cluster0.a5llf09.mongodb.net/Node-API?retryWrites=true&w=majority')
.then(() => {
    console.log('Connected to MongoDB')
    app.listen(3000, () => {  
        console.log('Node API app is running on port 3000')
    })
}).catch((error) => {
    console.log(error)
})
